<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

Auth::routes();

Route::group([
    'middleware' => ['auth'],
    'namespace' => 'Admin',
    'name' => 'admin.',
], function () {

    Route::get('/home', 'DashboardController@index')->name('home.index');
    Route::get('/cadastro-produto', 'DashboardController@cadastro')->name('cadastro.cadastro');
    Route::get('/produto/{id}/editar', 'DashboardController@editar')->name('produto.editar');
    Route::post('/salvar', 'ProdutoController@store')->name('salvar.store');
    Route::put('/atualizar-produto/{id}', 'ProdutoController@update')->name('produto.update');


});

