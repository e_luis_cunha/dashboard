@extends('layouts.dashboard')

@section('content')
<h4>Listagem de Produtos</h4>
<div class="table-responsive">
  <table class="table table-striped table-sm">
    <thead>
      <tr>
        <th>ID</th>
        <th>FOTO</th>
        <th>PREÇO</th>
        <th>AÇÃO</th>
      </tr>
    </thead>
    <tbody>

       
        @forelse ($lista as $item)
        <tr>
            <td>{{ $item->id }}</td>
            <td><img src="{{ url('image') . "/". $item->foto }}" alt="..." width="150px"></td>
            <td>R$ {{ number_format($item->valor, 2, ',', '.') }}</td>
            <td><a class="btn btn-sm btn-info" href="{{ route('produto.editar', $item->id) }}">Editar</a></td>
          </tr>
        @empty
        <tr>
            <td colspan="4">Nenhum Registro encontrado</td>
          </tr>
        @endforelse
      
      
    </tbody>
  </table>
</div>

@if (session('success'))
<p class="alert alert-success">
  {{ session('success') }}
</p>
@endif

@if (session('error'))
<p class="alert alert-danger">
  {{ session('error') }}
</p>
@endif
@endsection