@extends('layouts.dashboard')

@section('content')
<h4>Edição de Produto</h4>

<form action="{{ $url }}" method="POST" enctype="multipart/form-data">
        
    @csrf
    @method('PUT')
    <label for="">Nome</label> 
    <input type="text" name="nome" id="nome" value="{{ $nome }}" class="form-control" /> <br>
    <label for="">Preço</label>
    <input type="text" name="valor" id="valor" value="{{ $valor }}" class="form-control"> <br>
    <label for="">Descrição</label>
    <textarea name="descricao" id="descricao" cols="30" rows="10" class="form-control">{{ $descricao }}</textarea>
    <input type="hidden" name="foto" id="foto" value="{{ $foto }}">
    
    <br> <br>
    <button type="submit" class="btn btn-sm btn-primary">Salvar</button>
        
        
    
</form>
    
@endsection