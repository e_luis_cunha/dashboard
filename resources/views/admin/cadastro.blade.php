@extends('layouts.dashboard')

@section('content')


    <h4>Cadastro de Produto</h4>

    
    <form action="{{ $url }}" method="POST" enctype="multipart/form-data">
        
        @csrf
        {{-- @method('PUT') --}}
        <label for="">Nome</label> 
        <input type="text" name="nome" id="nome" value="{{ $nome }}" class="form-control" /> <br>
        <label for="">Preço</label>
        <input type="text" name="valor" id="valor" value="{{ $valor }}" class="form-control"> <br>
        <label for="">Descrição</label>
        <textarea name="descricao" id="descricao" cols="30" rows="10" class="form-control">{{ $descricao }}</textarea>
        <br>
        <input type="file" name="foto" id="foto">
        <br> <br>
        <button type="submit" class="btn btn-sm btn-primary">Salvar</button>
            
            
        
    </form>

    @if (session('success'))
       <p class="alert alert-success">
         {{ session('success') }}
       </p>
   @endif

   @if (session('error'))
       <p class="alert alert-danger">
         {{ session('error') }}
       </p>
   @endif
           
       

    
@endsection