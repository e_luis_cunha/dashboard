<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Module\Admin\ProdutoModel;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    //

    public function index(){

        $lista = ProdutoModel::get();
        return view('admin.dashboard', compact('lista'));
    }


    public function cadastro(){
        $nome = "";
        $descricao = "";
        $valor = "";
        $foto = "";
        $url = route('salvar.store');
        
        return view('admin.cadastro', compact('nome', 'descricao', 'valor', 'foto', 'url'));
    }

    public function editar($id){

        $editar = ProdutoModel::find($id);

        $nome = $editar->nome;
        $descricao = $editar->descricao;
        $valor = number_format($editar->valor, 2, ',', '.');
        $foto = $editar->foto;
        $url = route('produto.update', $id);
        
        // dd($editar);
        
        return view('admin.editar', compact('nome', 'descricao', 'valor', 'foto', 'url'));
    }

    // public function store(Request $request){
    //     dd($request);
    // }
}
