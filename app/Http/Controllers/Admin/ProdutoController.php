<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Module\Admin\ProdutoModel;
use Illuminate\Http\Request;

class ProdutoController extends Controller
{
    //

    public function index(){
        
    }

    public function store(Request $request, ProdutoModel $produto){

        $produto->nome = $request->nome;
        $produto->valor = str_replace(',','.',$request->valor);
       
        $produto->descricao = $request->descricao;


        if($request->hasFile('foto') && $request->file('foto')->isValid()){
            $img = $request->foto;
            $ext = $img->extension();
            $imgNome = md5($img->getClientOriginalName() . strtotime('NOW')) . "." . $ext;
            
            $img->move(public_path('image/'), $imgNome);
            $produto->foto = $imgNome;
        }

        $insert = $produto->save();
        if($insert){
            return redirect()
                    ->route('cadastro.cadastro')
                    ->with('success', 'Cadastrado efetuado com sucesso !!!');
        }else{
            return redirect()
                ->back()
                ->with('error', 'Erro ao cadastrar');
        }
        // dd($produto);
    }


    public function update(Request $request, $id){

        $produto = ProdutoModel::find($id);
        $produto->nome = $request->nome;
        $produto->valor = str_replace(',','.',$request->valor);
        $produto->foto = $request->foto;
        $produto->descricao = $request->descricao;

        $insert = $produto->save();
        if($insert){
            return redirect()
                    ->route('home.index')
                    ->with('success', 'Cadastrado atualizado com sucesso !!!');
        }else{
            return redirect()
                ->back()
                ->with('error', 'Erro ao atualizar');
        }
    }

}
