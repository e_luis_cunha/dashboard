<?php

namespace App\Module\Admin;

use Illuminate\Database\Eloquent\Model;

class ProdutoModel extends Model
{
    //
    protected $table = "produto";
    protected $fillable = ['nome', 'descricao', 'valor', 'foto'];
    
}
